<?php

function longest_flight_route($flights) {
    $maxDuration = 0;
    $maxRoute = [];
    foreach ($flights as $i => $flight1) {
        $depart1 = strtotime($flight1['depart']);
        $arrival1 = strtotime($flight1['arrival']);
        foreach ($flights as $j => $flight2) {
            if ($i == $j) continue;
            if ($flight1['to'] != $flight2['from']) continue;
            $depart2 = strtotime($flight2['depart']);
            $arrival2 = strtotime($flight2['arrival']);
            if ($arrival1 > $depart2) continue;
            $duration = $arrival2 - $depart1;
            if ($duration > $maxDuration) {
                $maxDuration = $duration;
                $maxRoute = [$flight1, $flight2];
            }
        }
    }
    return $maxRoute;
}

$flights = [
    [
        'from'    => 'VKO',
        'to'      => 'DME',
        'depart'  => '01.01.2020 12:44',
        'arrival' => '01.01.2020 13:44',
    ],
    [
        'from'    => 'DME',
        'to'      => 'JFK',
        'depart'  => '02.01.2020 23:00',
        'arrival' => '03.01.2020 11:44',
    ],
    [
        'from'    => 'DME',
        'to'      => 'HKT',
        'depart'  => '01.01.2020 13:40',
        'arrival' => '01.01.2020 22:22',
    ],
];

$longestRoute = longest_flight_route($flights);

echo "{$longestRoute[0]['depart']} по {$longestRoute[count($longestRoute)-1]['arrival']}";


